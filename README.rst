Python only wrapper for Telldus TCP
===============================

tellpy-tcp is a Python wrapper for `Telldus TCP <https://gitlab.com/droberg/telldus>`_.

Telldus TCP is a fork of `Telldus <https://github.com/telldus/telldus/>`_
home automation software with support for
* Direct TCP communication
* Mixing TellstickNet, TellstickDuo and TellstickNetv2
* Device-groups 


tellpy-tcp
--------
 
* Official home page: https://gitlab.com/droberg/telldus
* Python package index: https://pypi.python.org/ (Upcoming)
