# -*- coding: utf-8 -*-
# Copyright (c) 2023 Per Oberg <per@familjenoberg.se>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

"""
Contains the telldus API functions that interacts with a global TdClient client.
The client will be initialized as soon as any api function is called but to be
able to set the client ip address it is recommended to use the tdInit() as the first call.

Recomended usage

from tellpytcp import api as tdApi
tdApi.tdInit("192.168.x.y")
...
...
tdApi.tdClose()

Advanced usage

For more advanced configurations the server address and ports can be
setup individually by passing them as arguments to init.

from tellpytcp import api as tdApi
tdApi.tdSetServerAddress("192.168.x.y", clientPort=a, eventPort=b)

It is also possible to start and stop the event handling as follows

tdApi.tdStopEventHandler()
tdApi.tdStartEventHandler()

"""

from .client import TdClient
from .coremessage import TdCoreMessage
from .sensor import TdSensor
from .controller import TdController

from .constants import (
    TdErrorStrings,
    TELLSTICK_ALL,
    TELLSTICK_DEFAULT_SERVER,
    TELLSTICK_SUCCESS,
    TELLSTICK_ERROR_NETWORK_FAIL
)

import logging
logger = logging.getLogger(__name__)

# API FUNCTIONS
telldusService: TdClient


def tdInit(ipAddr: str = TELLSTICK_DEFAULT_SERVER, clientPort: int = -1,
           eventPort: int = -1) -> TdClient:
    global telldusService
    if "telldusService" not in globals():
        telldusService = TdClient(ipAddr, clientPort=clientPort,
                                  eventPort=eventPort)
    return telldusService


def tdClose():
    global telldusService
    if "telldusService" not in globals():
        return
    telldusService.close()
    del (telldusService)


def tdGetInstance() -> TdClient:
    global telldusService
    if "telldusService" in globals():
        return telldusService
    return tdInit()


def tdTurnOn(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdTurnOn")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdTurnOff(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdTurnOff")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdBell(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdBell")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdDim(intDeviceId: int, level: int):
    telldusService = tdGetInstance()
    if type(level) != int or level < 0 or level > 255:
        raise ValueError
    msg = TdCoreMessage("tdDim")
    msg.addArgument(intDeviceId)
    msg.addArgument(level)
    return telldusService.getIntegerFromService(msg)


def tdExecute(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdExecute")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdUp(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdUp")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdDown(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdDown")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdStop(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdStop")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdLearn(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdLearn")
    msg.addArgument(intDeviceId)
    return telldusService.getIntegerFromService(msg)


def tdMethods(id: int, methodsSupported: int = 1023):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdMethods")
    msg.addArgument(id)
    msg.addArgument(methodsSupported)
    return telldusService.getIntegerFromService(msg)


def tdLastSentCommand(intDeviceId: int, methodsSupported: int = 1023):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdLastSentCommand")
    msg.addArgument(intDeviceId)
    msg.addArgument(methodsSupported)
    return telldusService.getIntegerFromService(msg)


def tdLastSentValue(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdLastSentValue")
    msg.addArgument(intDeviceId)
    response = telldusService.getStringFromService(msg)
    return response


def tdGetNumberOfDevices() -> int:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetNumberOfDevices")
    response = telldusService.getIntegerFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetDeviceId(intDeviceIndex: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetDeviceId")
    msg.addArgument(intDeviceIndex)
    response = telldusService.getIntegerFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetDeviceType(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetDeviceType")
    msg.addArgument(intDeviceId)
    response = telldusService.getIntegerFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetPreferredControllerId(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetPreferredControllerId")
    msg.addArgument(intDeviceId)
    response = telldusService.getIntegerFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdSetPreferredControllerId(intDeviceId: int, preferredControllerId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetPreferredControllerId")
    msg.addArgument(intDeviceId)
    msg.addArgument(preferredControllerId)
    response = telldusService.getBoolFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetErrorString(intErrorNo: int):
    return TdErrorStrings[intErrorNo]


def tdGetName(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetName")
    msg.addArgument(intDeviceId)
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdSetName(intDeviceId: int, chNewName: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetName")
    msg.addArgument(intDeviceId)
    msg.addArgument(chNewName)
    response = telldusService.getBoolFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetProtocol(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetProtocol")
    msg.addArgument(intDeviceId)
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdSetProtocol(intDeviceId: int, strProtocol: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetProtocol")
    msg.addArgument(intDeviceId)
    msg.addArgument(strProtocol)
    response = telldusService.getBoolFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetModel(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetModel")
    msg.addArgument(intDeviceId)
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdSetModel(intDeviceId: int, strModel: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetModel")
    msg.addArgument(intDeviceId)
    msg.addArgument(strModel)
    response = telldusService.getBoolFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetDeviceParameter(intDeviceId: int, strName: str, defaultValue: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetDeviceParameter")
    msg.addArgument(intDeviceId)
    msg.addArgument(strName)
    msg.addArgument(defaultValue)
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdSetDeviceParameter(intDeviceId: int, strName: str, strValue: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetDeviceParameter")
    msg.addArgument(intDeviceId)
    msg.addArgument(strName)
    msg.addArgument(strValue)
    response = telldusService.getBoolFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdGetControllerStats(intDeviceId: int) -> str:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetControllerStats")
    msg.addArgument(intDeviceId)
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdClearControllerStats(intDeviceId: int) -> int:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdClearControllerStats")
    msg.addArgument(intDeviceId)
    response = telldusService.getIntegerFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    return response


def tdAddDevice() -> int:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdAddDevice")
    response = telldusService.getIntegerFromService(msg)
    return response


def tdRemoveDevice(intDeviceId: int) -> bool:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdRemoveDevice")
    response = telldusService.getBoolFromService(msg)
    return response


def tdSendRawCommand(command: str, reserved: int = 0):
    telldusService = tdGetInstance()
    print("Note: This is functionality is untested. YMMV")
    msg = TdCoreMessage("tdSendRawCommand")
    msg.addArgument(command)
    msg.addArgument(reserved)
    response = telldusService.getIntegerFromService(msg)
    return response


def tdConnectTellStickController(vid: int, pid: int, serial: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdConnectTellStickController")
    msg.addArgument(vid)
    msg.addArgument(pid)
    msg.addArgument(serial)
    telldusService.getStringFromService(msg)


def tdDisconnectTellStickController(vid: int, pid: int, serial: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdDisconnectTellStickController")
    msg.addArgument(vid)
    msg.addArgument(pid)
    msg.addArgument(serial)
    telldusService.getStringFromService(msg)


def tdSensor() -> list[TdSensor]:
    telldusService = tdGetInstance()
    sensorList: list[TdSensor] = []
    msg = TdCoreMessage("tdSensor")
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    if response == "":
        return sensorList
    else:
        msg = TdCoreMessage(response.encode("UTF-8"))
        numSensors = msg.takeInt()
        while numSensors > 0:
            sensorList.append(TdSensor())
            sensorList[-1].readFromMsg(msg)
            numSensors -= 1
        return sensorList


def tdSensorValue(sensor: TdSensor, dataType: int = TELLSTICK_ALL) -> dict:
    telldusService = tdGetInstance()
    from datetime import datetime
    sensorValues = {}
    for sensorType in sensor.supportedTypes(dataType):
        msg = sensor.createMsg(sensorType)
        response = TdCoreMessage(telldusService.getStringFromService(msg).encode())
        logger.debug("Response = \"{}\"".format(response))
        val = response.takeString()
        ts = response.takeInt()

        sensorValues[sensorType] = {'val': val, 'timestamp': ts}
        logger.debug("UTC: " + datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'))
        logger.debug("Value age: " + str(datetime.utcnow() - datetime.utcfromtimestamp(ts)))
    return sensorValues


def tdGetSensorName(intDeviceId: int) -> str:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetSensorName")
    msg.addArgument(intDeviceId)
    return telldusService.getStringFromService(msg)


def tdSetSensorName(intDeviceId: int, chNewName: str) -> bool:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetSensorName")
    msg.addArgument(intDeviceId)
    msg.addArgument(chNewName)
    return telldusService.getBoolFromService(msg)


def tdGetSensorProtocol(intDeviceId: int) -> str:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetSensorProtocol")
    msg.addArgument(intDeviceId)
    return telldusService.getStringFromService(msg)


def tdGetSensorModel(intDeviceId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetSensorModel")
    msg.addArgument(intDeviceId)
    return telldusService.getStringFromService(msg)


def tdController() -> list[TdController]:
    telldusService = tdGetInstance()
    controllerList: list[TdController] = []
    msg = TdCoreMessage("tdController")
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    if response == "":
        return controllerList
    else:
        msg = TdCoreMessage(response.encode("UTF-8"))
        numControllers = msg.takeInt()
        while numControllers > 0:
            controllerList.append(TdController())
            controllerList[-1].readFromMsg(msg)
            numControllers -= 1
        return controllerList


def tdControllerValue(controller: TdController, paramName: str = "") -> str:
    telldusService = tdGetInstance()
    msg = controller.createMsg(paramName)
    response = telldusService.getStringFromService(msg)
    return response


def tdSetControllerValue(id: int, name: str, value: str) -> int:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetControllerValue")
    msg.addArgument(id)
    msg.addArgument(name)
    msg.addArgument(value)
    return telldusService.getIntegerFromService(msg)


def tdRemoveController(controllerId: int):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdRemoveController")
    msg.addArgument(controllerId)
    return telldusService.getIntegerFromService(msg)


def tdStartEventHandler():
    telldusService = tdGetInstance()
    telldusService.eventHandler.start()


def tdStopEventHandler():
    telldusService = tdGetInstance()
    telldusService.eventHandler.stop()


def tdPing() -> int:
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdPing")
    response = telldusService.getStringFromService(msg)
    logger.debug("Response = \"{}\"".format(response))
    if response == "tdPong":
        return TELLSTICK_SUCCESS
    else:
        return TELLSTICK_ERROR_NETWORK_FAIL


def tdGetGlobalSetting(name: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdGetGlobalSetting")
    msg.addArgument(name)
    strReturn = telldusService.getStringFromService(msg)
    return strReturn


def tdSetGlobalSetting(name: str, value: str):
    telldusService = tdGetInstance()
    msg = TdCoreMessage("tdSetGlobalSetting")
    msg.addArgument(name)
    msg.addArgument(value)
    return telldusService.getIntegerFromService(msg)
