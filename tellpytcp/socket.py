# Copyright (c) 2023 Per Öberg <per@familjenoberg.se>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

"""
Contains socket class used by the client and the eventhandler classes.
"""

from .constants import (
    TELLSTICK_DEFAULT_SERVER,
    TELLSTICK_DEFAULT_CLIENTPORT,
    TELLSTICK_MAX_CONNECTION_ATTEMPTS,
    TELLSTICK_CONNECTION_TIMEOUT,
)

from .coremessage import (
    TdCoreMessage
)

import logging
logger = logging.getLogger(__name__)


class TdSocket():
    def __init__(self, serverAddress=TELLSTICK_DEFAULT_SERVER, port=TELLSTICK_DEFAULT_CLIENTPORT):
        self.isInitialized = False
        self.setServerAddress(serverAddress, port)
        self.connectionAttempts = 0

    def setServerAddress(self, serverAddress, port, doConnect=False):
        self.serverAddress = (serverAddress, port)
        if doConnect:
            self.connectSocket()

    def connectSocket(self):
        import socket
        import time
        if self.hasFault():
            logger.info("Socket faulted, clear to retry")
            return
        if self.isInitialized:
            self.sock.close()
        self.isInitialized = True

        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connectionAttempts += 1
        logger.info('Connecting to {} port {} ({} of {})'.format(self.serverAddress[0], self.serverAddress[1], self.connectionAttempts, TELLSTICK_MAX_CONNECTION_ATTEMPTS))

        # Try connect with a timeout
        self.sock.settimeout(TELLSTICK_CONNECTION_TIMEOUT)
        try:
            self.sock.connect(self.serverAddress)
            # self.clearFault()
        except ConnectionRefusedError as error:
            logger.warning("Error: {}".format(error))
        except TimeoutError as error:
            logger.warning("Error: {}".format(error))
        except OSError as error:
            if error.errno == 113:
                logger.warning("Error: {}".format(error.strerror))
            else:
                raise error
        else:
            # Operate with non blocking read/write
            self.sock.settimeout(0.0)
            return
        if not self.hasFault():
            time.sleep(TELLSTICK_CONNECTION_TIMEOUT)
            self.connectSocket()

    def hasFault(self):
        return self.connectionAttempts >= TELLSTICK_MAX_CONNECTION_ATTEMPTS

    def clearFault(self):
        self.connectionAttempts = 0

    def close(self):
        self.sock.close()
        self.isInitialized = False

    def checkConnect(self):
        if not self.isInitialized or self.sock.fileno() < 0:
            self.connectSocket()

    def tryWait(self, timeout: float = TELLSTICK_CONNECTION_TIMEOUT) -> bool:
        import select
        self.checkConnect()
        if self.hasFault():
            return False
        try:
            ready = select.select([self.sock], [], [], timeout)
        except ValueError:
            if not self.hasFault():
                if self.tryWait(timeout):
                    self.clearFault()
                    return True
            else:
                return False
        return len(ready[0]) > 0

    def tryRead(self) -> bytes:
        self.checkConnect()
        byteData = bytearray()
        if self.hasFault():
            return byteData
        try:
            bytes = bytearray()
            while True:
                bytes = self.sock.recv(4096)
                byteData.extend(bytes)
                if len(bytes) == 0:
                    break
        except BlockingIOError:
            if len(byteData) > 0:
                self.clearFault()
                return byteData
        logger.warning("Read error: Closing socket and throwing away any data ({} bytes)".format(len(byteData)))
        self.close()
        if self.connectionAttempts < TELLSTICK_MAX_CONNECTION_ATTEMPTS:
            return self.tryRead()
        else:
            return bytearray()

    def trySend(self, msg: TdCoreMessage):
        self.checkConnect()
        if self.hasFault():
            return False
        totalBytes = 0
        try:
            while totalBytes < len(msg):
                sentBytes = self.sock.send(msg[totalBytes:])
                totalBytes = totalBytes + sentBytes
                if sentBytes == 0:
                    logger.warn("Socket experienced BrokenPipeError")
                    raise BrokenPipeError
        except BrokenPipeError:
            logger.warn("Caught broken pipe, reconnecting")
            self.close()
            self.trySend(msg)
        return True
