#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .coremessage import TdCoreMessage

import logging
logger = logging.getLogger(__name__)


class TdController():
    def __init__(self, id: int = -1, type: int = -1, name: str = ""):
        self.id = id
        self.type = type
        self.name = name

    def readFromMsg(self, msg: TdCoreMessage):
        self.id = msg.takeInt()
        self.type = msg.takeInt()
        self.name = msg.takeString()
        self.available = msg.takeInt()
        logger.debug("Controller is: {}".format(self))

    def __repr__(self):
        return "Controller: id = {}, type = {}, name = {}, available = {}".format(self.id, self.type, self.name, self.available)

    def createMsg(self, name: str) -> TdCoreMessage:
        msg = TdCoreMessage("tdControllerValue")
        msg.addArgument(self.id)
        msg.addArgument(name)
        return msg
