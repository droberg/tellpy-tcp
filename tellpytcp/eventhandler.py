# Copyright (c) 2023 Per Öberg <per@familjenoberg.se>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

"""
Contains the event handler class used to process incoming events from the Telldus TCP server.
"""


from typing import Callable
from typing import Any

from .coremessage import TdCoreMessage
from .socket import TdSocket
from .constants import (
    TELLSTICK_SUCCESS,
    TELLSTICK_ERROR_NOT_FOUND,
    TELLSTICK_EVENTHANDLER_TIMEOUT,
)

import logging
logger = logging.getLogger(__name__)

# Handler function types
# DeviveEvent: int deviceId, int method, const char *data, int callbackId
TDDeviceEventFun = Callable[[int, int, bytearray, int], Any]
# DeviceChangeEvent: int deviceId, int changeEvent, int changeType, int callbackId
TDDeviceChangeEventFun = Callable[[int, int, int, int], Any]
# RawDeviceEvent: const char *data, int controllerId, int callbackId
TDRawDeviceEventFun = Callable[[bytearray, int, int], Any]
# SensorEvent: const char *protocol, const char *model, const char* name, int id, int dataType,
#               const char *value, int timestamp, int callbackId
TDSensorEventFun = Callable[[bytearray, bytearray, bytearray, int, int, bytearray, int, int], Any]
# SensorChangeEvent: int deviceId, int changeEvent, int changeType, int callbackId
TDSensorChangeEventFun = Callable[[int, int, int, int], Any]
# ControllerEvent: int controllerId, int changeEvent, int changeType, const char *newValue,
#                   int callbackId
TDControllerEventFun = Callable[[int, int, int, bytearray, int], Any]
# SettingsChangeEvent: iint changeType, int callbackId
TDSettingsChangeEventFun = Callable[[int, int], Any]


class TdEventHandler():
    # Handler list types
    devicEventHandlers: list[tuple[TDDeviceEventFun, int]]
    devicChangeEventHandlers: list[tuple[TDDeviceChangeEventFun, int]]
    rawDeviceEventHandlers: list[tuple[TDRawDeviceEventFun, int]]
    sensorEventHandlers: list[tuple[TDSensorEventFun, int]]
    sensorChangeEventHandlers: list[tuple[TDSensorChangeEventFun, int]]
    controllerEventHandlers: list[tuple[TDControllerEventFun, int]]
    settingsChangeEventHandlers: list[tuple[TDSettingsChangeEventFun, int]]
    handlerLists: list[list[tuple[Any, int]]]
    # Other types
    lastCallbackId: int
    msgBuffer: TdCoreMessage
    sock: TdSocket

    def __init__(self, socket: TdSocket):
        self.sock = socket
        self.devicEventHandlers = []
        self.devicChangeEventHandlers = []
        self.rawDeviceEventHandlers = []
        self.sensorEventHandlers = []
        self.sensorChangeEventHandlers = []
        self.controllerEventHandlers = []
        self.settingsChangeEventHandlers = []
        self.handlerLists = [self.devicEventHandlers,
                             self.devicChangeEventHandlers,
                             self.rawDeviceEventHandlers,
                             self.sensorEventHandlers,
                             self.sensorChangeEventHandlers,
                             self.controllerEventHandlers,
                             self.settingsChangeEventHandlers]
        self.lastCallbackId = 0
        self.msgBuffer = TdCoreMessage()
        self.__initThread__()
        self.start()

    def start(self):
        if self.isRunning:
            logger.warning("Event handler already running")
            return
        self.isRunning = True
        self.eventParser.start()

    def stop(self):
        self.isRunning = False
        if self.eventParser.is_alive():
            self.eventParser.join()
        self.sock.close()
        self.sock.clearFault()
        self.__initThread__()

    def __worker__(self):
        try:
            while self.isRunning:
                if not self.__readAllEvents__():
                    logger.warning("Socket fault, exiting event handler thread")
                    self.isRunning = False
                self.__parseAllEvents__()
        finally:
            self.isRunning = False

    def __initThread__(self):
        import threading
        self.isRunning = False
        self.eventParser = threading.Thread(target=self.__worker__)

    def __readAllEvents__(self) -> bool:
        if not self.sock.tryWait(TELLSTICK_EVENTHANDLER_TIMEOUT):
            if self.sock.hasFault():
                logger.warning("Socket fault")
                return False
            else:
                logger.debug("Event handler timed out, retrying later")  # Timeout waiting for data, retry later
                return True
        byteData = self.sock.tryRead()
        if len(byteData) >= 0:
            self.msgBuffer.rawExtend(byteData)
        return not self.sock.hasFault()

    def __parseAllEvents__(self):
        while self.msgBuffer:
            try:
                firstMsg = self.msgBuffer.pop(0)
                if firstMsg[0] != str:
                    logger.warning("Error in data processing - Message buffer does not start with string")
                    raise ValueError
                eventType = firstMsg[1]
                maxNumParameters = len(self.msgBuffer)
                data = dict()
                if maxNumParameters >= 1:
                    # At least eventType + 1 parameter
                    if eventType == "TDSettingsChangeEvent":
                        if not self.msgBuffer[0][1] == int:
                            raise ValueError
                        data["changeType"] = self.msgBuffer.pop(0)[1]
                        self.executeSettingsChangeEvent(data)
                        continue
                if maxNumParameters >= 2:
                    # At least eventType + 2 parameters
                    if eventType == "TDRawDeviceEvent":
                        if not [x[0] for x in self.msgBuffer[0:2]] == [str, int]:
                            raise ValueError
                        data["data"] = self.msgBuffer.pop(0)[1]
                        data["controllerId"] = self.msgBuffer.pop(0)[1]
                        self.executeRawDeviceEvent(data)
                        continue
                if maxNumParameters >= 3:
                    # At least eventType + 3 parameters
                    if eventType == "TDDeviceChangeEvent":
                        if not [x[0] for x in self.msgBuffer[0:3]] == [int, int, int]:
                            raise ValueError
                        data["deviceId"] = self.msgBuffer.pop(0)[1]
                        data["changeEvent"] = self.msgBuffer.pop(0)[1]
                        data["changeType"] = self.msgBuffer.pop(0)[1]
                        self.executeDeviceChangeEvent(data)
                        continue
                    if eventType == "TDDeviceEvent":
                        if not [x[0] for x in self.msgBuffer[0:3]] == [int, int, str]:
                            raise ValueError
                        data["deviceId"] = self.msgBuffer.pop(0)[1]
                        data["deviceState"] = self.msgBuffer.pop(0)[1]
                        data["deviceStateValue"] = self.msgBuffer.pop(0)[1]
                        self.executeDeviceEvent(data)  # If these calls takes time we will block here, possibly fix
                        continue
                    if eventType == "TDSensorChangeEvent":
                        if not [x[0] for x in self.msgBuffer[0:3]] == [int, int, int]:
                            raise ValueError
                        data["sensorId"] = self.msgBuffer.pop(0)[1]
                        data["changeEvent"] = self.msgBuffer.pop(0)[1]
                        data["changeType"] = self.msgBuffer.pop(0)[1]
                        self.executeSensorChangeEvent(data)
                        continue
                if maxNumParameters >= 4:
                    # At least eventType + 4 parameters
                    if eventType == "TDControllerEvent":
                        if not [x[0] for x in self.msgBuffer[0:4]] == [int, int, int, str]:
                            raise ValueError
                        data["controllerId"] = self.msgBuffer.pop(0)[1]
                        data["changeEvent"] = self.msgBuffer.pop(0)[1]
                        data["changeType"] = self.msgBuffer.pop(0)[1]
                        data["newValue"] = self.msgBuffer.pop(0)[1]
                        self.executeControllerEvent(data)
                        continue
                if maxNumParameters >= 7:
                    # At least eventType + 7 parameters
                    if eventType == "TDSensorEvent":
                        if not [x[0] for x in self.msgBuffer[0:7]] == [str, str, str, int, int, str, int]:
                            raise ValueError
                        data["protocol"] = self.msgBuffer.pop(0)[1]
                        data["model"] = self.msgBuffer.pop(0)[1]
                        data["name"] = self.msgBuffer.pop(0)[1]
                        data["id"] = self.msgBuffer.pop(0)[1]
                        data["dataType"] = self.msgBuffer.pop(0)[1]
                        data["value"] = self.msgBuffer.pop(0)[1]
                        data["timestamp"] = self.msgBuffer.pop(0)[1]
                        self.executeSensorEvent(data)
                        continue
                    else:
                        # If no message was recognized at this point, more messages will not be of any help later
                        logger.warning("Unhandled data, garbled ? Close and reconnect")
                        self.msgBuffer.clear()
                        self.sock.close()
                        break
                # Readd first message
                self.msgBuffer.insert(0, firstMsg)
                logger.warning("Cannot parse complete event yet, this is unusual, possibly more data to come later.")
                break
            except ValueError:
                logger.error("Unhandled data, garbled ? Please close, discard and reconnect")

    def registerDeviceEvent(self, fun: TDDeviceEventFun) -> int:
        self.lastCallbackId += 1
        self.devicEventHandlers.append((fun, self.lastCallbackId))
        return self.lastCallbackId

    def registerDeviceChangeEvent(self, fun: TDDeviceChangeEventFun) -> int:
        self.lastCallbackId += 1
        self.devicChangeEventHandlers.append((fun, self.lastCallbackId))
        return self.lastCallbackId

    def registerRawDeviceEvent(self, fun: TDRawDeviceEventFun) -> int:
        self.lastCallbackId += 1
        self.rawDeviceEventHandlers.append((fun, self.lastCallbackId))
        return self.lastCallbackId

    def registerSensorEvent(self, fun: TDSensorEventFun) -> int:
        self.lastCallbackId += 1
        self.sensorEventHandlers.append((fun, self.lastCallbackId))
        return self.lastCallbackId

    def registerControllerEvent(self, fun: TDControllerEventFun) -> int:
        self.lastCallbackId += 1
        self.controllerEventHandlers.append((fun, self.lastCallbackId))
        return self.lastCallbackId

    def registerSensorChangeEvent(self, fun: TDSensorChangeEventFun) -> int:
        self.lastCallbackId += 1
        self.sensorChangeEventHandlers.append((fun, self.lastCallbackId))
        return self.lastCallbackId

    def registerSettingsChangeEvent(self, fun: TDSettingsChangeEventFun) -> int:
        self.lastCallbackId += 1
        self.settingsChangeEventHandlers.append((fun, self.lastCallbackId))
        return self.lastCallbackId

    def unregisterCallback(self, callbackId: int):
        for eventHandlerList in self.handlerLists:
            for handler in eventHandlerList:
                if handler[1] == callbackId:
                    eventHandlerList.remove(handler)
                    return TELLSTICK_SUCCESS
        return TELLSTICK_ERROR_NOT_FOUND

    def executeDeviceEvent(self, data):
        for handler in self.devicEventHandlers:
            handler[0](data["deviceId"], data["deviceState"], data["deviceStateValue"], handler[1])

    def executeDeviceChangeEvent(self, data):
        for handler in self.devicChangeEventHandlers:
            handler[0](data["deviceId"], data["changeEvent"], data["changeType"], handler[1])

    def executeRawDeviceEvent(self, data):
        for handler in self.rawDeviceEventHandlers:
            handler[0](data["data"], data["controllerId"], handler[1])

    def executeSensorEvent(self, data):
        for handler in self.sensorEventHandlers:
            handler[0](data["protocol"], data["model"], data["name"], data["id"], data["dataType"], data["value"], data["timestamp"], handler[1])

    def executeControllerEvent(self, data):
        for handler in self.controllerEventHandlers:
            handler[0](data["controllerId"], data["changeEvent"], data["changeType"], data["newValue"], handler[1])

    def executeSensorChangeEvent(self, data):
        for handler in self.sensorChangeEventHandlers:
            handler[0](data["sensorId"], data["changeEvent"], data["changeType"], handler[1])

    def executeSettingsChangeEvent(self, data):
        for handler in self.settingsChangeEventHandlers:
            handler[0](data["changeType"], handler[1])
