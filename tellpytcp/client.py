# Copyright (c) 2023 Per Öberg <per@familjenoberg.se>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

"""
Contains a client used to communicate with the Telldus TCP server.
"""

from .constants import (
    TELLSTICK_DEFAULT_SERVER,
    TELLSTICK_DEFAULT_CLIENTPORT,
    TELLSTICK_DEFAULT_EVENTPORT,
    TELLSTICK_ERROR_CONNECTING_SERVICE,
    TELLSTICK_ERROR_COMMUNICATING_SERVICE,
    TELLSTICK_SUCCESS
)

from .socket import TdSocket
from .eventhandler import TdEventHandler
from .coremessage import TdCoreMessage

import logging
logger = logging.getLogger(__name__)


class TdClient():
    import select
    from collections import UserString

    def __init__(self, serverAddress=TELLSTICK_DEFAULT_SERVER, clientPort=-1, eventPort=-1):
        if clientPort == -1:
            clientPort = TELLSTICK_DEFAULT_CLIENTPORT
        if eventPort == -1:
            eventPort = TELLSTICK_DEFAULT_EVENTPORT
        self.clientSock = TdSocket(serverAddress, clientPort)
        self.clientSock.connectSocket()
        self.eventSock = TdSocket(serverAddress, eventPort)
        self.eventHandler = TdEventHandler(self.eventSock)

    def setServerAddress(self, serverAddress, clientPort, eventPort):
        if clientPort == -1:
            clientPort = TELLSTICK_DEFAULT_CLIENTPORT
        if eventPort == -1:
            eventPort = TELLSTICK_DEFAULT_EVENTPORT
        logger.debug("Setting address to: {}".format(serverAddress))
        self.clientSock.setServerAddress(serverAddress, clientPort, doConnect=True)
        self.eventSock.setServerAddress(serverAddress, eventPort, doConnect=True)

    def close(self):
        self.eventHandler.stop()
        self.clientSock.close()

    def sendToService(self, msg: TdCoreMessage) -> TdCoreMessage:
        readData = TdCoreMessage()

        if not self.clientSock.trySend(msg.encode()):
            readData.addArgument(TELLSTICK_ERROR_CONNECTING_SERVICE)
            return readData

        if not self.clientSock.tryWait():
            readData.addArgument(TELLSTICK_ERROR_CONNECTING_SERVICE)
            return readData

        byteData = self.clientSock.tryRead()
        logger.debug("Received byte data: {!r}".format(byteData))
        readData = TdCoreMessage(byteData)
        if readData == TdCoreMessage():
            readData.addArgument(TELLSTICK_ERROR_CONNECTING_SERVICE)

        return readData

    def getIntegerFromService(self, msg) -> int:
        response = self.sendToService(msg)
        logger.debug("Integer response : {}".format(response))
        if response == "":
            return TELLSTICK_ERROR_COMMUNICATING_SERVICE
        else:
            return response.takeInt()

    def getStringFromService(self, msg: TdCoreMessage) -> str:
        response = self.sendToService(msg)
        respString = ""
        logger.debug("String response : {}".format(response))
        if response.nextIsString():
            respString = response.takeString()
        elif response.nextIsInt():
            respInt = response.takeInt()
            if respInt != 0:  # If return string is empty a single zero is returned so this is weird
                logger.warning("Expected string or zero, got: {}".format(respInt))
                # Re-add argument that is possibly for another query
                response.addArgument(respInt)
        return respString

    def getBoolFromService(self, msg: TdCoreMessage) -> bool:
        return self.getIntegerFromService(msg) == TELLSTICK_SUCCESS
