#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Type aliases for message and arguments
TdCoreArgBaseType = tuple[type, str]
TdCoreMessageBaseType = list[TdCoreArgBaseType]


class TdCoreArg(TdCoreArgBaseType):
    '''Argument of TdCoreMessage with both tuple[type,str] and bytearray representation built in.
    \nMessage is stored as tuple but can be encoded on the fly
    \nThe argument can be initialized in the following ways:
    * Using a string argument
    * Using an interger argument
    * From a bytearray representing the encoded argument'''

    def __new__(self, arg: str | int | bytearray | bytes = ''):
        if type(arg) not in [str, int, bytearray, bytes]:
            raise TypeError
        elif isinstance(arg, (bytearray, bytes)):
            decArg, residue = self.decode(arg)  # Throw away residue here
            return tuple.__new__(TdCoreArg, (type(decArg), decArg))
        return tuple.__new__(TdCoreArg, (type(arg), arg))

    def encode(self):
        '''Encodes TdCoreArg into bytearray'''
        res = bytearray()
        if self[0] == int:
            res = ("i" + str(self[1]) + "s").encode("UTF-8")
        elif self[0] == str and self[1]:
            res = (str(len(self[1])) + ":" + self[1]).encode("UTF-8")
        return res

    @staticmethod
    def decode(arg: bytearray | bytes):
        '''arg, residue = TdCoreArg::decode(raw)\n
        Decodes bytearray input to TdCoreArg.\n
        If there is parts left after the decode they will be returned as a second argument'''
        strArg = arg.decode("UTF-8")
        if len(strArg) < 2:
            raise ValueError
        if strArg[0].isdigit():
            # Argument is string, search for separator ":"
            index = strArg.find(":")
            strLen = 0
            if index >= 1:
                strLen = int(strArg[0:index])
                if strLen > len(strArg) - 2:
                    raise ValueError
            else:
                raise ValueError
            residue = strArg[index + strLen + 1:].encode('UTF-8')
            if isinstance(arg, bytearray):
                arg.clear()
                arg.extend(residue)
            return strArg[index + 1: index + strLen + 1], residue
        elif strArg[0] == 'i':
            # Argument is integer
            index = strArg.find('s')
            if index >= 1:
                value = int(strArg[1: index])
                residue = strArg[index + 1:].encode('UTF-8')
                if isinstance(arg, bytearray):
                    arg.clear()
                    arg.extend(residue)
                return value, residue
            else:
                raise ValueError
        else:
            raise ValueError


class TdCoreMessage(TdCoreMessageBaseType):
    def __init__(self, arg: bytearray | bytes | str | TdCoreArg | None = None):
        self.raw = bytearray()
        if isinstance(arg, (bytearray, bytes)):
            if isinstance(arg, bytes):
                self.raw = bytearray(arg)
            else:
                self.raw = arg
            self.__decode()
        elif isinstance(arg, (str, int)):
            self.append(TdCoreArg(arg))
        elif isinstance(arg, TdCoreArg):
            self.append(arg)
        elif arg is None:
            return
        else:
            raise TypeError

    def __decode(self):
        '''Read TdCoreMessageBaseType from bytearray data'''
        while len(self.raw) > 0:
            try:
                coreArg = TdCoreArg(self.raw)
                self.append(coreArg)
            except ValueError:
                break

    def rawExtend(self, arg: bytearray | bytes):
        self.raw.extend(arg)
        self.__decode()

    def addArgument(self, arg: str | int | TdCoreArg = ''):
        '''Add argument to message list'''
        if isinstance(arg, (str, int)):
            self.append(TdCoreArg(arg))
        elif isinstance(arg, TdCoreArg):
            self.append(arg)
        else:
            raise TypeError
        return self

    def nextIsString(self):
        return len(self) > 0 and self[0][0] == str

    def takeString(self):
        if self.nextIsString():
            return self.pop(0)[1]
        else:
            raise ValueError

    def nextIsInt(self):
        return len(self) > 0 and self[0][0] == int

    def takeInt(self):
        if self.nextIsInt():
            return self.pop(0)[1]
        else:
            raise ValueError

    def encode(self):
        '''Get bytearray from message list'''
        res = bytearray()
        for arg in self:
            res.extend(arg.encode())
        return res
