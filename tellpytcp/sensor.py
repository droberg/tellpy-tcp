#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .coremessage import TdCoreMessage
from .constants import (
    TELLSTICK_ALL,
    TELLSTICK_TEMPERATURE,
    TELLSTICK_HUMIDITY,
    TELLSTICK_RAINRATE,
    TELLSTICK_RAINTOTAL,
    TELLSTICK_WINDDIRECTION,
    TELLSTICK_WINDAVERAGE,
    TELLSTICK_WINDGUST,
    TELLSTICK_BATTERY,
)

import logging
logger = logging.getLogger(__name__)


class TdSensor():
    def __init__(self, protocol: str = "", model: str = "", name: str = "", dataTypes: int = 0, id: int = -1):
        self.protocol = protocol
        self.model = model
        self.id = id
        self.name = name
        self.dataTypes = dataTypes

    def readFromMsg(self, msg: TdCoreMessage):
        self.protocol = msg.takeString()
        self.model = msg.takeString()
        self.name = msg.takeString()
        self.id = msg.takeInt()
        self.dataTypes = msg.takeInt()
        logger.debug("Sensor: {}".format(self))

    def __repr__(self):
        return "Sensor: id = {}, datatypes = {}, protocol = {}, model = {}, name = {}".format(self.id, self.dataTypes, self.protocol, self.model, self.name)

    def supportedTypes(self, requestedType: int = TELLSTICK_ALL) -> list[int]:
        retVal: list[int] = []
        dataTypes = self.dataTypes & requestedType
        if dataTypes & TELLSTICK_TEMPERATURE:
            retVal.append(TELLSTICK_TEMPERATURE)
        if dataTypes & TELLSTICK_HUMIDITY:
            retVal.append(TELLSTICK_HUMIDITY)
        if dataTypes & TELLSTICK_RAINRATE:
            retVal.append(TELLSTICK_RAINRATE)
        if dataTypes & TELLSTICK_RAINTOTAL:
            retVal.append(TELLSTICK_RAINTOTAL)
        if dataTypes & TELLSTICK_WINDDIRECTION:
            retVal.append(TELLSTICK_WINDDIRECTION)
        if dataTypes & TELLSTICK_WINDAVERAGE:
            retVal.append(TELLSTICK_WINDAVERAGE)
        if dataTypes & TELLSTICK_WINDGUST:
            retVal.append(TELLSTICK_WINDGUST)
        if dataTypes & TELLSTICK_BATTERY:
            retVal.append(TELLSTICK_BATTERY)
        return retVal

    def createMsg(self, sensorType: int) -> TdCoreMessage:
        msg = TdCoreMessage("tdSensorValue")
        msg.addArgument(self.protocol)
        msg.addArgument(self.model)
        msg.addArgument(self.id)
        msg.addArgument(sensorType)
        return msg

    @staticmethod
    def stringToType(type: str) -> int:
        intType: int = 0
        if type == "temp":
            intType = TELLSTICK_TEMPERATURE
        elif type == "humidity":
            intType = TELLSTICK_HUMIDITY
        elif type == "rainrate":
            intType = TELLSTICK_RAINRATE
        elif type == "raintotal":
            intType = TELLSTICK_RAINTOTAL
        elif type == "winddirecetion":
            intType = TELLSTICK_WINDDIRECTION
        elif type == "windaverage":
            intType = TELLSTICK_WINDAVERAGE
        elif type == "windgust":
            intType = TELLSTICK_WINDGUST
        return intType
