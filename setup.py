#!/usr/bin/env python

from distutils.core import setup
import os
import re

setup(
    name='tellpytcp',
    version="0.1",
    author='Per Öberg',
    author_email='per@familjenoberg.se',
    packages=['tellpytcp'],
    provides=['tellpytcp'],
    scripts=[],
    url='https://gitlab.com/droberg/tellpytcp',
    license='GPLv3+',
    description='Native Python client for telldus-tcp (telldus server with tcp support)',
    long_description=open('README.rst').read() + '\n\n' +
        open('CHANGES.rst').read(),
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Home Automation',
        'Topic :: Software Development :: Libraries :: Python Modules',
        ]
)
