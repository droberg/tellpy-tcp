#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# %%% Import necessary libraries
import time
import sys

from tellpytcp import api as tdApi

from tellpytcp.constants import (
    TELLSTICK_TURNON,
    TELLSTICK_TURNOFF,
)

import logging
logging.basicConfig()

logger = logging.getLogger()
logger.setLevel("DEBUG")


# %%% Define example class for handlign device
class ExampleDeviceObject():
    OnOff: bool

    def __init__(self):
        self.OnOff = False

    def exampleDeviceEventFun(self, deviceId: int, method: int, data: bytearray, callbackId: int):
        print("Id = {}, Method = {}, data = {}".format(deviceId, method, data))
        if method == TELLSTICK_TURNON:
            self.OnOff = True
        elif method == TELLSTICK_TURNOFF:
            self.OnOff = False

    def exampleSensorEventFun(self, protocol, model, name, id, dataType, value, timestamp, callbackId):
        print("protocol = {}, model = {}, name = {}, id = {}, dataType = {}, value = {}, timestamp = {}".format(protocol, model, name, id, dataType, value, timestamp))

    def exampleRawEventFun(self, data: bytearray, controllerId: int, callbackId: int):
        print(data)


exampleObject = ExampleDeviceObject()


# %%% Initialize api, test connection, get server instance, and register callbacks
def start(serverAddress):
    global deviceEventCallbackId
    global sensorEventCallbackId
    global rawEventCallbackId
    global srv

    tdApi.tdInit(serverAddress)
    tdApi.tdPing()

    srv = tdApi.tdGetInstance()

    deviceEventCallbackId = srv.eventHandler.registerDeviceEvent(exampleObject.exampleDeviceEventFun)
    sensorEventCallbackId = srv.eventHandler.registerSensorEvent(exampleObject.exampleSensorEventFun)
    rawEventCallbackId = srv.eventHandler.registerRawDeviceEvent(exampleObject.exampleRawEventFun)


# %%% Define main that waits for Ctrl-C and then removes callbacks and close connection
if __name__ == "__main__":
    global deviceEventCallbackId
    global sensorEventCallbackId
    global rawEventCallbackId
    global srv

    if len(sys.argv) < 2:
        print("Usage: {} serverIP".format(sys.argv[0]))
        exit(0)
    try:
        print("Use Ctrl-C to exit! \n")
        start(sys.argv[1])
        while srv.eventHandler.isRunning:
            time.sleep(0.5)
    except KeyboardInterrupt:
        srv.eventHandler.unregisterCallback(deviceEventCallbackId)
        srv.eventHandler.unregisterCallback(sensorEventCallbackId)
        srv.eventHandler.unregisterCallback(rawEventCallbackId)
        tdApi.tdClose()
    finally:
        print("Exit")
