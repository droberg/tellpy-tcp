# -*- coding: utf-8 -*-
'''
Support library for testing tellpy-tcp

Created on Sat Mar 18 07:51:31 2023

@author: Per Öberg
'''


from tellpytcp.constants import (
    # Sensor types
    TELLSTICK_TEMPERATURE,
    TELLSTICK_HUMIDITY,
    # TELLSTICK_RAINRATE,
    # TELLSTICK_RAINTOTAL,
    # TELLSTICK_WINDDIRECTION,
    # TELLSTICK_WINDAVERAGE,
    # TELLSTICK_WINDGUST,
    TELLSTICK_BATTERY,
    # TELLSTICK_ALL,
)


from tellpytcp.constants import (
    # Controller typedef
    # TELLSTICK_CONTROLLER_TELLSTICK,
    TELLSTICK_CONTROLLER_TELLSTICK_DUO,
    # TELLSTICK_CONTROLLER_TELLSTICK_NET,
)

from tellpytcp.constants import (
    # Device methods
    TELLSTICK_TURNON,
    TELLSTICK_TURNOFF,
    # TELLSTICK_BELL,
    # TELLSTICK_TOGGLE,
    # TELLSTICK_DIM,
    TELLSTICK_LEARN,
    # TELLSTICK_EXECUTE,
    # TELLSTICK_UP,
    # TELLSTICK_DOWN,
    # TELLSTICK_STOP,
    # TELLSTICK_UNDEFINED,
)

from tellpytcp.constants import (
    # Device typedef
    TELLSTICK_TYPE_DEVICE,
    # TELLSTICK_TYPE_GROUP,
    # TELLSTICK_TYPE_SCENE,
    # TELLSTICK_TYPE_DEVICEGROUP,
)

testSensor = {'id': 63,
              'name': 'TestSensorName',
              'expectedValues':
                  {TELLSTICK_TEMPERATURE: {'valueMin': -5, 'valueMax': 50},
                   TELLSTICK_HUMIDITY: {'valueMin': 10, 'valueMax': 99},
                   TELLSTICK_BATTERY: {'valueMin': 255, 'valueMax': 255},
                   },
              'supportedTypes': [TELLSTICK_TEMPERATURE,
                                 TELLSTICK_HUMIDITY,
                                 TELLSTICK_BATTERY,
                                 ]
              }


testController = {'id': 1,
                  'type': TELLSTICK_CONTROLLER_TELLSTICK_DUO,
                  'name': 'TestControllerName',
                  'serial': 'ABCD1234'
                  }


testDevice = {'id': 7,
              'name': 'TestDeviceName',
              'lastCmd': TELLSTICK_TURNON,
              'lastVal': '',
              'preferredCtl': 0,
              'methods': TELLSTICK_TURNON + TELLSTICK_TURNOFF + TELLSTICK_LEARN,
              'protocol': 'arctech',
              'model': 'selflearning-switch:nexa',
              'type': TELLSTICK_TYPE_DEVICE}


class EventCallbackRecorder():
    callbackLog: dict[str, list]

    def __init__(self):
        self.callbackLog = dict()
        self.callbackLog['deviceEvents'] = []
        self.callbackLog['sensorEvents'] = []
        self.callbackLog['rawEvents'] = []
        pass

    def deviceEventRecorderFun(self, deviceId: int, method: int, data: bytearray, callbackId: int):
        entry = {'id': deviceId, 'method': method, 'data': data}
        self.callbackLog['deviceEvents'] += entry
        print('Adding entry:', entry)

    def sensorEventRecorderFun(self, protocol, model, name, id, dataType, value, timestamp, callbackId):
        entry = {'protocol': protocol, 'model': model, 'name': name, 'id': id, 'dataType': dataType, 'value': value, 'timestamp': timestamp}
        self.callbackLog['sensorEvents'] += entry
        print('Adding entry:', entry)

    def rawEventRecorderFun(self, data: bytearray, controllerId: int, callbackId: int):
        entry = {'data': data, 'controllerId': controllerId, 'callbackId': callbackId}
        self.callbackLog['rawEvents'] += entry
        print('Adding entry:', entry)
