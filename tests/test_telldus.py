# -*- coding: utf-8 -*-
# Copyright (c) 2023 Per Öberg <per@familjenoberg.se>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA

import unittest

import gc
from tellpytcp import api as tdApi

from tellpytcp.constants import (
    TELLSTICK_SUCCESS,
    # Sensor types
)

from tellpytcp.client import TdClient
from tellpytcp.coremessage import TdCoreMessage

from support import EventCallbackRecorder
from support import (
    testSensor,
    testController,
    testDevice,
)


class Test(unittest.TestCase):
    def setUp(self):
        tdApi.tdInit()
        self.srv = tdApi.tdGetInstance()

    def tearDown(self):
        tdApi.tdClose()
        del self.srv
        gc.collect()

    def test_tdPing(self):
        print('Testing tdPing')
        res = tdApi.tdPing()
        self.assertEqual(res, TELLSTICK_SUCCESS)

    def test_tdDeviceCmnds(self):
        import time
        print('Testing tdTurnOn, Off, etc')
        res = tdApi.tdGetNumberOfDevices()
        self.assertGreaterEqual(res, 1)
        deviceList = []
        for deviceNum in range(0, res):
            deviceId = tdApi.tdGetDeviceId(deviceNum)
            entry = {'id': deviceId,
                     'name': tdApi.tdGetName(deviceId),
                     'lastCmd': tdApi.tdLastSentCommand(deviceId),
                     'lastVal': tdApi.tdLastSentValue(deviceId),
                     'preferredCtl': tdApi.tdGetPreferredControllerId(deviceId),
                     'methods': tdApi.tdMethods(deviceId),
                     'protocol': tdApi.tdGetProtocol(deviceId),
                     'model': tdApi.tdGetModel(deviceId),
                     'type': tdApi.tdGetDeviceType(deviceId)
                     }
            deviceList += [entry]

        indexList = [device['id'] for device in deviceList]
        self.assertIn(testDevice['id'], indexList)

        testId = testDevice['id']
        tdApi.tdTurnOn(testId)
        self.assertEqual(tdApi.tdLastSentCommand(testId), testDevice['lastCmd'])
        tdApi.tdTurnOff(testId)
        time.sleep(0.5)
        self.assertNotEqual(tdApi.tdLastSentCommand(testId), testDevice['lastCmd'])

    def test_connRefused(self):
        print("Testing connetion refused")
        srv = TdClient('192.0.2.0', clientPort=123, eventPort=124)
        self.assertTrue(srv.clientSock.hasFault())
        srv = TdClient('127.0.0.1', clientPort=123, eventPort=124)
        self.assertTrue(srv.clientSock.hasFault())
        self.assertFalse(srv.clientSock.trySend(TdCoreMessage('tdPing')))
        srv.close()

    def test_listSensors(self):
        print('Testing sensor api')
        sensorsList = tdApi.tdSensor()
        sensorIds = [sensor.id for sensor in sensorsList]
        self.assertGreaterEqual(len(sensorsList), 1)
        self.assertIn(testSensor['id'], sensorIds)
        self.assertEqual(sensorsList[sensorIds.index(testSensor['id'])].name, testSensor['name'])
        self.assertEqual(sensorsList[sensorIds.index(testSensor['id'])].supportedTypes(), testSensor['supportedTypes'])

    def test_sensorValue(self):
        print("Test sensor values")
        sensorsList = tdApi.tdSensor()
        sensorIds = [sensor.id for sensor in sensorsList]
        self.assertIn(testSensor['id'], sensorIds)
        sensorValues = tdApi.tdSensorValue(sensorsList[sensorIds.index(testSensor['id'])])
        for sensorType in testSensor['expectedValues']:
            valueMin = testSensor['expectedValues'][sensorType]['valueMin']
            valueMax = testSensor['expectedValues'][sensorType]['valueMax']
            self.assertGreaterEqual(float(sensorValues[sensorType]['val']), valueMin)
            self.assertLessEqual(float(sensorValues[sensorType]['val']), valueMax)

    def test_listControllers(self):
        print("Testing controller api")
        controllerList = tdApi.tdController()
        controllerIds = [controller.id for controller in controllerList]
        controller = controllerList[controllerIds.index(testController['id'])]
        serial = tdApi.tdControllerValue(controller, 'serial')
        self.assertIn(testController['id'], controllerIds)
        self.assertEqual(controller.type, testController['type'])
        self.assertEqual(serial, testController['serial'])

    def test_eventCallbacks(self):
        import time
        print('Testing event callbacks')
        rec = EventCallbackRecorder()
        srv = tdApi.tdGetInstance()
        deviceEventCallbackId = srv.eventHandler.registerDeviceEvent(rec.deviceEventRecorderFun)
        sensorEventCallbackId = srv.eventHandler.registerSensorEvent(rec.sensorEventRecorderFun)
        rawEventCallbackId = srv.eventHandler.registerRawDeviceEvent(rec.rawEventRecorderFun)

        for i in range(0, 10):
            print('Waiting for events: ', 10 - i)
            time.sleep(2)

        srv.eventHandler.unregisterCallback(deviceEventCallbackId)
        srv.eventHandler.unregisterCallback(sensorEventCallbackId)
        srv.eventHandler.unregisterCallback(rawEventCallbackId)


if __name__ == '__main__':
    unittest.main()
