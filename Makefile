COVRUN=coverage run
COVPOST=coverage html

all:

.PHONY: tests
tests:
#	python2 ./run_tests
	${COVRUN} ./run_tests.py
	${COVPOST}
#	pypy ./run_tests
#       Ignore E501: "Line to long"
#       Ignore E722: "Use of bare except"
	flake8 --ignore=E501,E722 tellpytcp
	flake8 --ignore=E501,E722 tests
	mypy tellpytcp

